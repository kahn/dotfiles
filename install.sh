#!/bin/bash

mkdir -p $HOME/.config
mkdir -p $HOME/.config/i3
mkdir -p $HOME/.config/gtk-3.0
mkdir -p $HOME/.config/termite
mkdir -p $HOME/.config/lemonbuddy
mkdir -p $HOME/.config/ranger

# i3
ln -s $HOME/.dotfiles/.config/i3/config $HOME/.config/i3/config

# Termite
ln -s $HOME/.dotfiles/.config/termite/config  $HOME/.config/termite/config
ln -s $HOME/.dotfiles/.config/gtk-3.0/gtk.css $HOME/.config/gtk-3.0/gtk.css

# Lemonbuddy
ln -s $HOME/.dotfiles/.config/lemonbuddy/config   $HOME/.config/lemonbuddy/config
ln -s $HOME/.dotfiles/.config/lemonbuddy/start.sh $HOME/.config/lemonbuddy/start.sh

# Ranger
ln -s $HOME/.dotfiles/.config/ranger/rc.conf $HOME/.config/ranger/rc.conf

ln -s $HOME/.dotfiles/.Xresources $HOME/.Xresources
ln -s $HOME/.dotfiles/.tmux.conf $HOME/.tmux.conf

xrdb -merge $HOME/.Xresources
