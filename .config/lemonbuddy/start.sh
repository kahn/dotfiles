#!/usr/bin/env bash

# If another launcher is already running it probably failed
# to shutdown previous processes, send SIGKILL
# Exit since the running script will launch the new processes
if pgrep -f "bash $0" | grep -v $$ >/dev/null; then
  killall -q -9 polybar; exit
fi

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

if [[ "$1" == "kill" ]]; then
      exit
fi

if xrandr | grep -q "HDMI1 connected"; then
  polybar leftBar -l info -c $HOME/.config/polybar/config &
  polybar rightBar -l info -c $HOME/.config/polybar/config &
  sleep 1;
  bspc config top_padding 40
else
  polybar centerBar -c $HOME/.config/polybar/config &
  sleep 1;
  bspc config top_padding 40
fi


echo Bars launched...
