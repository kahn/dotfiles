set -gx PATH $HOME/bin /usr/local/bin $HOME/.local/bin /usr/bin/core_perl $PATH
xrdb -merge $HOME/.Xresources

eval sh $HOME/.config/base16-shell/base16-ocean.dark.sh

alias sctl "systemctl"
alias jctl "journalctl"
alias la "ls -hlaFL --group-directories-first"
alias ll "ls -hlFL --group-directories-first"

set fisher_home ~/.local/share/fisherman
set fisher_config ~/.config/fisherman
set XDG_CURRENT_DESKTOP kde dolphin
source $fisher_home/config.fish
# start X at login
#if status --is-login
#    if test -z "$DISPLAY" -a $XDG_VTNR -eq 1
#        exec startx -- -keeptty
#    end
#end
